import React, { Component } from "react";
import { connect } from "react-redux";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

import config from '../../config';
import {
  Button,
  Col,
  PageHeader, Row
} from "react-bootstrap";
import { facebookLogin, } from "../../store/actions/users";
import { Icon } from "semantic-ui-react";

class Login extends Component {

  facebookResponse = response => {
    if (response.id) {
      this.props.facebookLogin(response);
    }
  };

  render() {
    return (
      <div className="container">
        <Row>
          <Col mdOffset={4} md={4}>
            <PageHeader>Login with Facebook
              <FacebookLogin
                appId={config.facebookAppId}
                fields="name,email,picture"
                render={renderProps => (
                  <Button onClick={renderProps.onClick}
                          style={{backgroundColor: '#3b5998', color: '#fff',
                            margin: '10px 0 10px 20px'}}>
                    <Icon name='facebook' size='large'/>Login
                  </Button>
                )}
                callback={this.facebookResponse}
              />
            </PageHeader>
          </Col>
        </Row>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.users.loginError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    facebookLogin: data => dispatch(facebookLogin(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
