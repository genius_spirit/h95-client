import React, { Component } from "react";
import { connect } from "react-redux";

import { getTasks } from "../../store/actions/tasks"

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';

class Tasks extends Component {
  componentDidMount() {
    this.props.getTasks();
  }

  render() {
    return (
      <div className="container">
        <CssBaseline />
        {this.props.tasks &&
          this.props.tasks.map(item => (
            <Paper elevation={4} key={item._id} style={{padding: '15px', margin: '20px'}}>
              <Typography style={{}}>
                <p><strong>DateTime: {item.startDatetime}</strong></p>
              </Typography>
              <Typography variant="headline" component="h3" >
                {item.title}
              </Typography>
              <Typography component="p">
                <span>From: {item.startDatetime}</span>
                <span>To: {item.endDatetime}</span>
              </Typography>
            </Paper>
            )
          )
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.tasks.tasks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getTasks: () => dispatch(getTasks())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
