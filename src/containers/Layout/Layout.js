import React, { Fragment } from "react";
import { connect } from "react-redux";
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Toolbar from "../../components/UI/Toolbar/Toolbar";
import { logoutUser } from "../../store/actions/users";
import AddNewTask from "../../components/AddNewTask/AddNewTask";

const Layout = props => (
  <Fragment>
    <NotificationContainer/>
    <header>
      <Toolbar user={props.user} logout={props.logoutUser}/>
    </header>
    <section>
      {props.user && <AddNewTask/>}
    </section>
    <main className="container">
      {props.children}
    </main>
  </Fragment>
);

const mapStateToProps = state => {
  return {
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch(logoutUser())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
