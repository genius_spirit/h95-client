import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import Tasks from "./containers/Tasks/Tasks";
import Login from "./containers/Login/Login";

const Routes = ({ user }) => {
  return (
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/tasks" exact component={Tasks} />
      <Route
        render={() => <h1 style={{ textAlign: "center" }}>Page not found</h1>}
      />
    </Switch>
  );
};

const mapStateToProps = state => {
  return {
    user: state.users.user
  };
};

export default withRouter(connect(mapStateToProps)(Routes));
