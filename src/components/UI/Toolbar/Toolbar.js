import React from "react";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";


const Toolbar = ({ user, logout }) => {

  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <LinkContainer to="/">
            <a>Schedule</a>
          </LinkContainer>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        {user && (
          <Nav pullRight activeKey={1}>
            <NavItem eventKey={2} onClick={logout}>LOGOUT</NavItem>
          </Nav>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Toolbar;
