import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import { connect } from "react-redux";
import { addNewTask } from "../../store/actions/tasks";

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: '20px',
    marginRight: '20px',
    width: 200,
    marginTop: 0
  },
  button: {
    margin: theme.spacing.unit,
  }
});

class AddNewTask extends Component {

  state = {
    title: '',
    startDatetime: '',
    endDatetime: ''
  };

  handleChange = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  handleSubmit = event => {
    event.preventDefault();
    const data = {title: this.state.title, startDatetime: this.state.startDatetime, endDatetime: this.state.endDatetime};
    this.props.addNewTask(data);
  };

render() {
  const { classes } = this.props;
  return (
    <div className="container">

    <form className={classes.container} noValidate onSubmit={this.handleSubmit}>

      <TextField
        name="title"
        label="Title"
        className={classes.textField}
        value={this.state.title}
        onChange={this.handleChange}
        margin="normal"
        style={{width: '500px'}}
      />

      <TextField
        name="startDatetime"
        label="startDatetime"
        type="datetime-local"
        defaultValue="2017-05-24T10:30"
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        value={this.state.startDatetime}
        onChange={this.handleChange}
      />

      <TextField
        name="endDatetime"
        label="endDatetime"
        type="datetime-local"
        defaultValue="2017-05-24T10:30"
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        value={this.state.endDatetime}
        onChange={this.handleChange}
      />

      <Button variant="contained" className={classes.button} type="submit">
        Add
      </Button>

    </form>
    </div>
  );
}
}

const mapDispatchToProps = dispatch => {
  return {
    addNewTask: task => dispatch(addNewTask(task))
  }
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(AddNewTask));
