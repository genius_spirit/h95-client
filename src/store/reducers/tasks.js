import { ADD_NEW_TASK_FAILURE, GET_TASKS_FAILURE, GET_TASKS_SUCCESS } from "../actions/actionTypes";

const initialstate = {
  tasks: [],
  taskError: ''
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case ADD_NEW_TASK_FAILURE:
      return {...state, taskError: action.error};
    case GET_TASKS_SUCCESS:
      return {...state, tasks: action.data};
    case GET_TASKS_FAILURE:
      return {...state, taskError: action.error};
    default:
      return state;
  }
};

export default reducer;