import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import { facebookLoginSuccess, facebookLoginFailure, logoutUserSuccess } from "../actions/users";
import { push } from "react-router-redux";
import { NotificationManager } from "react-notifications";

export function* facebookLoginSaga(action) {
  try {
    const response = yield axios.post('/users/facebookLogin', action.data);
    if (response) {
      yield put(facebookLoginSuccess(response.data.user, response.data.token));
      NotificationManager.success('Success', "LogIn with Facebook");
      yield put(push("/tasks"));
    }
  } catch (error) {
    yield put(facebookLoginFailure(error.response.data));
  }
}

export function* logoutUserSaga() {
  try {
    yield axios.delete('/users/sessions');
    yield put(logoutUserSuccess());
    yield put(push("/"));
    NotificationManager.success('Success', "Successfully LogOut");
  } catch (e) {
    NotificationManager.error('Error', "Logout failed");
  }
}