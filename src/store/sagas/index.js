import {takeEvery, all} from 'redux-saga/effects';
import { ADD_NEW_TASK, FACEBOOK_LOGIN, GET_TASKS, LOGOUT_USER } from "../actions/actionTypes";
import { facebookLoginSaga, logoutUserSaga } from "./users";
import { addNewTaskSaga, getTasksSaga } from "./tasks";

function* facebookLogin() {
  yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga)
}

function* logoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
}

function* addNewTask() {
  yield takeEvery(ADD_NEW_TASK, addNewTaskSaga);
}

function* getTasks() {
  yield takeEvery(GET_TASKS, getTasksSaga);
}

export function* rootSaga() {
  yield all([
    facebookLogin(),
    logoutUser(),
    addNewTask(),
    getTasks()
  ])
}