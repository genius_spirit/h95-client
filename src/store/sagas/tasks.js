import axios from "../../axios-api";
import {put} from 'redux-saga/effects'

import { addNewTaskFailure, addNewTaskSuccess, getTasksFailure, getTasksSuccess } from "../actions/tasks";

export function* addNewTaskSaga(action) {
  try {
    yield axios.post('/tasks', action.data);
    yield put(addNewTaskSuccess);
  } catch (e) {
    yield put(addNewTaskFailure(e.response.data));
  }
}

export function* getTasksSaga() {
  try {
    const response = yield axios.get('/tasks');
    if (response) yield put(getTasksSuccess(response.data));
  } catch (e) {
    yield put(getTasksFailure(e.response.data));
  }
}