import {
  ADD_NEW_TASK,
  ADD_NEW_TASK_FAILURE,
  ADD_NEW_TASK_SUCCESS, GET_TASKS,
  GET_TASKS_FAILURE,
  GET_TASKS_SUCCESS
} from "./actionTypes";

export const addNewTaskSuccess = () => {
  return {type: ADD_NEW_TASK_SUCCESS};
};

export const addNewTaskFailure = error => {
  return {type: ADD_NEW_TASK_FAILURE, error};
};

export const addNewTask = data => {
  return {type: ADD_NEW_TASK, data};
};

export const getTasksSuccess = data => {
  return {type: GET_TASKS_SUCCESS, data};
};

export const getTasksFailure = error => {
  return {type: GET_TASKS_FAILURE, error};
};

export const getTasks = () => {
  return {type: GET_TASKS};
};