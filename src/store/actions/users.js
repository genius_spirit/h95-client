import { push } from "react-router-redux";
import {NotificationManager} from 'react-notifications';

import {
  FACEBOOK_LOGIN,
  FACEBOOK_LOGIN_FAILURE,
  FACEBOOK_LOGIN_SUCCESS,
  LOGOUT_USER, LOGOUT_USER_SUCCESS
} from "./actionTypes";

export const logoutUser = () => {
  return {type: LOGOUT_USER};
};

export const logoutUserSuccess = () => {
  return {type: LOGOUT_USER_SUCCESS};
};

export const logoutExpiredUser = () => {
  return dispatch => {
    dispatch({type: LOGOUT_USER_SUCCESS});
      dispatch(push('/'));
      NotificationManager.error('Error', 'Expired time of session')
  }
};

export const facebookLoginSuccess = (user, token) => {
  return { type: FACEBOOK_LOGIN_SUCCESS, user, token };
};

export const facebookLoginFailure = error => {
  return { type: FACEBOOK_LOGIN_FAILURE, error };
};

export const facebookLogin = data => {
  return {type: FACEBOOK_LOGIN, data};

};